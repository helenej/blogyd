<?php
/* Constantes pour la connexion a la base de donnees */
define('HOSTNAME', 'localhost');
define('DBNAME', 'test');
define('DSN', 'mysql:host=' . HOSTNAME . ';dbname=' . DBNAME);
define('USERNAME', 'root');
define('PASSWD', '');

/* Constantes symbolisant les pages */
define('PAGE_ACCUEIL', '1');
define('PAGE_RAISONS_DAGIR', '2');
define('PAGE_ACTIONS', '3');
define('PAGE_UN_AUTRE_BRON', '4');
define('PAGE_CONTACT', '5');

// Nombre d'articles a afficher par page
define('NB_ARTICLES_PAR_PAGE', '3');

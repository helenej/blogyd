<?php
function soumettre_formulaire() {
    $filtres = array('nom' => FILTER_SANITIZE_STRING, // Nettoie le nom
            'prenom' => FILTER_SANITIZE_STRING, // Nettoie le prénom
            'email' => FILTER_SANITIZE_EMAIL, // Nettoie l'adresse E-mail
    );
    $champs = filter_input_array(INPUT_POST, $filtres);

    if (!empty($champs['email'])) {
        $champs['email'] = filter_var($champs['email'], FILTER_VALIDATE_EMAIL);
        // Valide l'E-mail
    }

    $donnees = array();
    if (!is_null($champs)) {// Si le formulaire a bien été posté
        $donnees['champs'] = $champs;

        $actions = array('agir_t' => array('Agir sur le terrain', false),
                'agir_w' => array('Agir sur le web', false),
                'redacteur' => array('Devenir rédacteur', false),
                'informe' => array('Se tenir informé', false),
                'donner' => array('Donner à l\'association', false),
                'savoir' => array('En savoir plus', false));
        foreach ($actions as $nom => $valeur) {
            if (filter_has_var(INPUT_POST, $nom)) {
                $actions[$nom][1] = true;
            }
        }
        $donnees['actions'] = $actions;
    }

    return $donnees;
}

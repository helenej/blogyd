<?php
function recuperer_article($p_id = null) {
    $req = "SELECT id AS current_id, titre, DATE_FORMAT(date_creation, '%d/%m/%Y, %Hh%imin%ss') AS date_creation_fr, page, contenu, image
            FROM billets";

    // Si l'id est precise on recupere seulement l'article correspondant
    if (!is_null($p_id)) {
        $req = $req . ' WHERE id = ' . $p_id;
    }

    $req = $req . ' ORDER BY date_creation DESC';

    $res = BDD::getInstance()->query($req);

    // On cree un tableau d'articles qu'on remplit avec toutes les lignes de la table billets
    $articles = array();
    $ligne = $res->fetch(PDO::FETCH_ASSOC);
    while ($ligne) {
        $articles[] = $ligne;
        $ligne = $res->fetch(PDO::FETCH_ASSOC);
    }
    $res->closeCursor();

    return $articles;
}

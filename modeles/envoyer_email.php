<?php
function envoyer_email($p_to, $p_from, $p_sujet, $p_message_txt, $p_message_html) {
    // On filtre les serveurs qui rencontrent des bogues.
    if (!preg_match('#^[a-z0-9._-]+@(hotmail|live|msn).[a-z]{2,4}$#', $p_to)) {
        $passage_ligne = "\r\n";
    } else {
        $passage_ligne = "\n";
    }

    //=====Création de la boundary
    $boundary = '-----=' . md5(rand());
    //==========

    //=====Création du header de l'e-mail.
    $header = 'From: "' . $p_from['nom'] . '"<' . $p_from['email'] . '>' . $passage_ligne;
    $header .= 'MIME-Version: 1.0' . $passage_ligne;
    $header .= 'Content-Type: multipart/alternative;' . $passage_ligne . ' boundary="' . $boundary . '"' . $passage_ligne;
    //==========

    //=====Création du message.
    $message = $passage_ligne . '--' . $boundary . $passage_ligne;
    //=====Ajout du message au format texte.
    $message .= 'Content-Type: text/plain; charset="utf-8"' . $passage_ligne;
    $message .= 'Content-Transfer-Encoding: 8bit' . $passage_ligne;
    $message .= $passage_ligne . $p_message_txt . $passage_ligne;
    //==========
    $message .= $passage_ligne . '--' . $boundary . $passage_ligne;
    //=====Ajout du message au format HTML
    $message .= 'Content-Type: text/html; charset="utf-8"' . $passage_ligne;
    $message .= 'Content-Transfer-Encoding: 8bit' . $passage_ligne;
    $message .= $passage_ligne . $p_message_html . $passage_ligne;
    //==========
    $message .= $passage_ligne . '--' . $boundary . '--' . $passage_ligne;
    $message .= $passage_ligne . '--' . $boundary . '--' . $passage_ligne;
    //==========

    //=====Envoi de l'e-mail.
    return mail($p_to, $p_sujet, $message, $header);
    //==========
}

<?php
function valider_formulaire($champs) {
    foreach ($champs as $nom => $valeur) {
        if (!$valeur) {
            return false;
        }
    }
    return chk_crypt($_POST['captcha']);
}

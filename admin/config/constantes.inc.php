<?php
/* Constantes pour la connexion a la base de donnees */
define('HOSTNAME', 'localhost');
define('DBNAME', 'test');
define('DSN', 'mysql:host=' . HOSTNAME . ';dbname=' . DBNAME);
define('USERNAME', 'root');
define('PASSWD', '');

/* Constantes pour gérer les erreurs */
define('PAS_DERREUR', '0');

define('ERREUR_TELECHARGEMENT', '1');
define('ERREUR_TAILLE', '2');
define('ERREUR_FORMAT', '3');
define('ERREUR_DEPLACEMENT', '4');

/* Constantes symbolisant les pages */
define('PAGE_ACCUEIL', '1');
define('PAGE_RAISONS_DAGIR', '2');
define('PAGE_ACTIONS', '3');
define('PAGE_UN_AUTRE_BRON', '4');
define('PAGE_CONTACT', '5');

define('NB_ARTICLES_PAR_PAGE', '3'); // Nombre d'articles a afficher par page
define('TAILLE_MAX_IMAGE', '3000000'); // Taille maximum des images (en octets)

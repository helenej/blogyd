<?php
require_once __DIR__ . '/../modeles/recuperer_article.php';

$articles = recuperer_article();

require_once __DIR__ . '/../modeles/trier_articles.php';

if (!empty($_GET['tri'])) {
    $articles = trier_articles($articles, $_GET['tri'], $_GET['dir']);
}

require_once __DIR__ . '/../vues/accueil.php';

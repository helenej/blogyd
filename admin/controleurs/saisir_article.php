<?php
// Si l'id est precise on recupere l'article correspondant afin de l'afficher dans le formulaire
if (!empty($_GET['id'])) {
    require_once './modeles/recuperer_article.php';

    $articles = recuperer_article($_GET['id']);
    $article = $articles[0];
}

require_once __DIR__ . '/../vues/saisir_article.php';

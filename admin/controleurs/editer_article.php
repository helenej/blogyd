<?php
require_once __DIR__ . '/../modeles/verifier_image.php';

// On verifie l'image pour l'insertion dans la base de donnees
$image_verifiee = verifier_image($_FILES['image']);

if ($image_verifiee['erreur'] == PAS_DERREUR) {
    if (!empty($_POST['id'])) {// Si l'id est precise c'est une mise-a-jour
        require_once __DIR__ . '/../modeles/modifier_article.php';

        if (!is_null($image_verifiee['nom'])) {
            require_once __DIR__ . '/../modeles/supprimer_image_article.php';

            supprimer_image_article($_POST['id']);
        }

        if (modifier_article($_POST['id'], $_POST['titre'], $_POST['contenu'], $_POST['page'], $image_verifiee['nom'])) {
            $message_edition = 'L\'article ' . $_POST['titre'] . ' a bien été modifié.';
        } else {
            $message_erreur = 'L\'article ' . $_POST['titre'] . ' n\'a pas pu être modifié.';
        }
    } else {// Sinon c'est une insertion
        require_once __DIR__ . '/../modeles/article_existe.php';

        // $id = BDD::getInstance()->lastInsertId();

        if (true) {
            require_once __DIR__ . '/../modeles/ajouter_article.php';

            if (ajouter_article($_POST['titre'], $_POST['contenu'], $_POST['page'], $image_verifiee['nom'])) {
                $message_edition = 'L\'article ' . $_POST['titre'] . ' a bien été ajouté.';
            } else {
                $message_erreur = 'L\'article ' . $_POST['titre'] . ' n\'a pas pu être ajouté.';
            }
        }
    }

    require_once __DIR__ . '/../modeles/recuperer_article.php';

    $articles = recuperer_article();

    require_once __DIR__ . '/../vues/accueil.php';
} else {
    switch ($image_verifiee['erreur']) {
        case 1 :
            $message_erreur = 'L\'image n\'a pas pu être téléchargée.';
            break;
        case 2 :
            $message_erreur = 'L\'image est trop grande.';
            break;
        case 3 :
            $message_erreur = 'Le format de l\'image est invalide.';
            break;
        case 4 :
            $message_erreur = 'L\'image n\'a pas pu être déplacée.';
            break;
    }

    // On remplit un tableau à la manière d'un article afin de pouvoir l'afficher dans le formulaire
    $article['titre'] = $_POST['titre'];
    $article['contenu'] = $_POST['contenu'];
    $article['page'] = $_POST['page'];

    require_once __DIR__ . '/../vues/saisir_article.php';
}

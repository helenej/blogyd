<?php
require_once __DIR__ . '/../modeles/article_existe.php';

if (article_existe($_GET['id'])) {

    require_once __DIR__ . '/../modeles/supprimer_image_article.php';

    supprimer_image_article($_GET['id']);

    require_once __DIR__ . '/../modeles/supprimer_article.php';

    supprimer_article($_GET['id']);
}

require_once __DIR__ . '/../modeles/recuperer_article.php';

$articles = recuperer_article();

require_once __DIR__ . '/../vues/accueil.php';

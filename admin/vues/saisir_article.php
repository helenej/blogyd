<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<link rel="stylesheet" href="./css/style.css" />
<title>Administration</title>
<script type="text/javascript" src="./js/nicEdit/nicEdit.js"></script>
<script type="text/javascript">
			bkLib.onDomLoaded(function() {
				new nicEditor({
					iconsPath : './js/nicEdit/nicEditorIcons.gif',
					buttonList : ['save', 'bold', 'italic', 'underline', 'ol', 'ul', 'fontSize', 'indent', 'outdent', 'upload', 'link', 'unlink']
				}).panelInstance('contenu');
			});
		</script>
<script type="text/javascript">
			function confirmer_suppression_image(arg) {
				return confirm('Etes-vous sur de vouloir supprimer l\'image ?')
			}
		</script>
</head>
<body>
 <div id="bloc_page">
  <header>
   <h1>Page d'administration du blog</h1>
  </header>
  <div id="bloc_contenu">
   <?php
   if (!empty($message_erreur))
       echo '
        <p id="message_erreur">
        ' . $message_erreur . '
        </p>
        ';
   ?>
   <table>
    <tr>
     <th>Editer un article</th>
     <th id="retour"><a href="./index.php">Retour à la liste des
       articles</a></th>
    </tr>
    <tr>
     <td colspan="2">
      <form action="./index.php?page=editer_article" method="post"
       enctype="multipart/form-data">
       <p>
        <label for="titre">Titre de l'article :</label> <input
         type="text" name="titre" id="titre" size="30"
         <?php
         if (!empty($article['titre']))
             echo 'value="' . $article['titre'] . '"';
         ?> />
       </p>
       <p>Contenu de l'article :</p>
       <p>
        <textarea name="contenu" id="contenu">
							<?php
							if (!empty($article['contenu']))
							    echo $article['contenu'];
							?>
							</textarea>
       </p>
       <p>
        <label for="image">Image pour l'article :</label> <input
         type="file" name="image" id="image" value="test" />
        <?php
        if (!empty($article['image'])) {
							?>
        <a
         href="./index.php?page=supprimer_image_article&id=<?php echo $article['current_id']; ?>"><button
          type="button"
          onclick="return(confirmer_suppression_image(''))">Supprimer
          l'ancienne image</button> </a>
        <?php
							}
							?>
       </p>
       <p>
        <label for="page">Page de l'article :</label><select name="page"
         id="page">
         <option value=<?php echo PAGE_ACTIONS; ?>
         <?php
         if (!empty($article['page']) && ($article['page'] == PAGE_ACTIONS))
             echo 'selected="selected"';
         ?>>Actions</option>
        </select>
       </p>
       <?php
       if (!empty($article['current_id'])) {
							?>
       <!-- Envoi de l'id de l'article de maniere invisible -->
       <input type="hidden" name="id"
        value=<?php echo $article['current_id']; ?> />
       <?php
							}
							?>
       <input type="submit" value="Editer" />
      </form>
     </td>
    </tr>
   </table>
  </div>
  <footer></footer>
 </div>
</body>
</html>

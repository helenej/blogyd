<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<link rel="stylesheet" href="./css/style.css" />
<title>Administration</title>
<script type="text/javascript">
			function confirmer_suppression_article(arg) {
				return confirm('Etes-vous sur de vouloir supprimer l\'article ' + arg + ' ?')
			}
		</script>
</head>

<body>
 <div id="bloc_page">
  <header>
   <h1>Page d'administration du blog</h1>
  </header>
  <div id="bloc_contenu">

   <?php
   if (!empty($message_edition)) {
					echo '
        <p>
        ' . $message_edition . '
        </p>';
				} else if (!empty($message_erreur)) {
					echo '
        <p id="message_erreur">
        ' . $message_erreur . '
				        </p>';
				}
				?>

   <table>
    <tr>
     <th><a
      href="./index.php?tri=date&dir=<?php
							if (isset($_GET['dir']) && $_GET['dir'] == 'asc')
								echo 'desc';
							else
								echo 'asc';
 ?>">Date de publication</a></th>
     <th><a
      href="./index.php?tri=titre&dir=<?php
							if (isset($_GET['dir']) && $_GET['dir'] == 'asc')
								echo 'desc';
							else
								echo 'asc';
 ?>">Titre</a></th>
     <th><a
      href="./index.php?tri=page&dir=<?php
							if (isset($_GET['dir']) && $_GET['dir'] == 'asc')
								echo 'desc';
							else
								echo 'asc';
 ?>">Page</a></th>
     <th></th>
     <th></th>
     <th></th>
    </tr>

    <?php
    if (empty($_GET['num_page'])) { // Si la variable $_GET['page'] n'est pas definie (premiere connexion), on est par défaut sur la page 1
$num_page = 1;
}
else { // Sinon la page courante est celle definie par la variable $_GET['page']
					$num_page = $_GET['num_page'];
					}

					for ($i = ($num_page - 1) * NB_ARTICLES_PAR_PAGE; $i <= ($num_page - 1) * NB_ARTICLES_PAR_PAGE + NB_ARTICLES_PAR_PAGE - 1; $i++) {
					if (!empty($articles[$i])) { // Si il y a un article a l'index $i
					?>

    <tr>
     <td><?php echo $articles[$i]['date_creation_fr']; ?></td>
     <td><?php echo htmlspecialchars($articles[$i]['titre']); ?></td>
     <td><?php
     if ($articles[$i]['page'] == PAGE_ACTIONS)
         echo 'Actions';
     ?>
     </td>
     <td><a
      href="./index.php?page=saisir_article&id=<?php echo $articles[$i]['current_id']; ?>"
      id="editer">Modifier</a></td>
     <td><a
      href="./../action-<?php echo $articles[$i]['current_id']; ?>-<?php echo $num_page; ?>.html#article"
      target="_blank" id="afficher">Afficher</a></td>
     <td><a
      href="./index.php?page=supprimer_article&id=<?php echo $articles[$i]['current_id']; ?>"
      onclick="return(confirmer_suppression_article('<?php echo $articles[$i]['titre']; ?>'
					))"
      id="supprimer">Supprimer</a></td>
    </tr>

    <?php
					} // Fin du if (!empty($articles[$i]))
					} // Fin de la boucle for
					?>
    <tr>
     <th></th>
     <th><a href="./index.php?page=saisir_article" id="ajouter">+
       Ajouter un article</a></th>
     <th></th>
     <th></th>
     <th></th>
     <th>Page : <?php
     // Si le tableau d'articles n'est pas vide
     if (!empty($articles)) {
						// Le nombre de pages est egal a la division entiere de sa taille par le nombre d'articles a afficher par page
						$nb_pages = ceil(sizeof($articles) / NB_ARTICLES_PAR_PAGE);

						for ($p = 1; $p <= $nb_pages; $p++) {
							if ($p == $num_page) {
								$num_page_url = $p;
							} else {
								if (!empty($_GET['tri'])) {
									$num_page_url = '<a href=\'./index.php?num_page=' . $p . '&tri=' . $_GET['tri'] . '&dir=' . $_GET['dir'] . ' \'>' . $p . '</a>  ';
								} else {
									$num_page_url = '<a href=\'./index.php?num_page=' . $p . ' \'>' . $p . '</a>  ';
								}
							}
							echo $num_page_url;
						} // Fin de la boucle for
					}// Fin du if (!empty($articles))
					else {
						echo "1";
					}
					?>
     </th>
    </tr>
   </table>
  </div>
  <footer></footer>
 </div>
</body>
</html>

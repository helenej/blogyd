<?php
function article_existe($p_id) {
    // On cherche l'article dans la base de donnees
    $req = 'SELECT COUNT(*) FROM billets WHERE id = ' . $p_id;
    $res = BDD::getInstance()->query($req);
    $nb_articles = $res->fetchColumn();
    $res->closeCursor();

    return $nb_articles > 0;
}

<?php
function modifier_article($p_id, $p_titre, $p_contenu, $p_page, $p_image) {
    $nouvelles_valeurs = array($p_titre, $p_contenu, $p_page); // Tableau contenant les nouvelles valeurs
    $req_aux = 'UPDATE billets SET titre = ?, contenu = ?, page = ?';
    if (!is_null($p_image)) {
        $req_aux .= ', image = ?';
        $nouvelles_valeurs[] = $p_image;
    }
    $req = BDD::getInstance()->prepare($req_aux . ' WHERE id = ' . $p_id);
    $modifie = $req->execute($nouvelles_valeurs);
    $req->closeCursor();

    return $modifie;
}

<?php
function ajouter_article($p_titre, $p_contenu, $p_page, $p_image) {
    $req = BDD::getInstance()->prepare('INSERT INTO billets (titre, contenu, date_creation, page, image) VALUES (?, ?, ?, ?, ?)');
    $ajoute = $req->execute(array($p_titre, $p_contenu, date(DATE_ISO8601), $p_page, $p_image));
    $req->closeCursor();

    return $ajoute;
}

<?php
function supprimer_image_article($p_id) {
    // On recupere l'ancienne image dans la base de donnees
    $req = 'SELECT image FROM billets WHERE id = ' . $p_id;
    $res = BDD::getInstance()->query($req);
    $image = $res->fetchColumn();
    $res->closeCursor();

    // On supprime l'ancienne image du dossier
    if (!is_null($image)) {
        if (unlink(__DIR__ . '/../../images/images_articles/' . $image)) {
            // On supprime l'ancienne image de la base de donnees
            $req = 'UPDATE billets SET image = NULL WHERE id = ' . $p_id;
            return BDD::getInstance()->exec($req);
        }
    }

    return false;
}

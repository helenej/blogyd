<?php
function verifier_image($p_image) {
    $image = array('nom' => null,
            'erreur' => PAS_DERREUR);

    // On verifie qu'une image a ete postee
    if (!empty($p_image['name'])) {
        // On genere un nom unique pour l'image
        // pour que deux images puissent avoir le meme nom
        $nouveau_nom_image = uniqid();

        // On teste s'il n'y a pas d'erreur
        if ($p_image['error'] == UPLOAD_ERR_OK) {

            // On teste si l'image n'est pas trop grosse
            if ($p_image['size'] <= TAILLE_MAX_IMAGE) {

                // On teste si l'extension est autorisee
                $infos_image = pathinfo($p_image['name']);
                $extension_image = $infos_image['extension'];
                $extensions_autorisees = array('jpg', 'jpeg', 'gif', 'png');
                if (in_array($extension_image, $extensions_autorisees)) {

                    $dir_image = __DIR__ . '/../../images/images_articles';
                    if (!is_dir($dir_image)) {
                        if (!is_writable(dirname($dir_image)) && !chmod(dirname($dir_image), 0777)) {
                            trigger_error('Impossible de creer le dossier de destination', E_USER_ERROR);
                        }
                        mkdir($dir_image);
                    }
                    if (move_uploaded_file($p_image['tmp_name'], $dir_image . '/' . basename($nouveau_nom_image . '.' . $extension_image))) {
                        $image['nom'] = $nouveau_nom_image . '.' . $extension_image;
                    } else {
                        $image['erreur'] = ERREUR_DEPLACEMENT;
                    }
                } else {
                    $image['erreur'] = ERREUR_FORMAT;
                }
            } else {
                $image['erreur'] = ERREUR_TAILLE;
            }
        } else {
            $image['erreur'] = ERREUR_TELECHARGEMENT;
        }
    }

    return $image;
}

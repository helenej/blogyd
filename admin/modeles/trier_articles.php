<?php
function trier_articles($p_articles, $p_tri, $p_dir) {
    if ($p_tri == 'titre') {
        foreach ($p_articles as $indice => $ligne) {
            $colonne[$indice] = strtolower(trim($ligne['titre']));
        }
    } elseif ($p_tri == 'date') {
        foreach ($p_articles as $indice => $ligne) {
            $colonne[$indice] = $ligne['date_creation_fr'];
        }
    } elseif ($p_tri == 'page') {
        foreach ($p_articles as $indice => $ligne) {
            $colonne[$indice] = $ligne['page'];
        }
    }

    if ($p_dir == 'asc') {
        array_multisort($colonne, SORT_ASC, $p_articles);
    } elseif ($p_dir == 'desc') {
        array_multisort($colonne, SORT_DESC, $p_articles);
    }

    return $p_articles;
}

function confirmer_suppression_article(arg) {
	return confirm('Etes-vous sur de vouloir supprimer l\'article ' + arg + ' ?');
}

function confirmer_suppression_image(arg) {
	return confirm('Etes-vous sur de vouloir supprimer l\'image ?');
}
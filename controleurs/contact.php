<?php
$page_courante = PAGE_CONTACT;

session_start();
$_SESSION['cryptdir'] = './library/crypt';

$cryptinstall = './library/crypt/cryptographp.fct.php';
require_once $cryptinstall;

require_once __DIR__ . '/../modeles/soumettre_formulaire.php';

$donnees = soumettre_formulaire();

if (!empty($donnees)) {
    require_once __DIR__ . '/../modeles/valider_formulaire.php';

    $cryptinstall = __DIR__ . '/../library/crypt/cryptographp.fct.php';
    require_once $cryptinstall;

    $form_est_valide = valider_formulaire($donnees['champs']);

    if ($form_est_valide) {
        $to = 'helene.jonin@gmail.com';
        // Déclaration de l'adresse de destination A CHANGER !!!

        $from = array('nom' => $donnees['champs']['prenom'] . ' ' . $donnees['champs']['nom'],
                'email' => $donnees['champs']['email']);

        $sujet = 'Formulaire de contact "A vous d\'agir !"';

        $liste_txt = '';
        $liste_html = '<ul>';
        foreach ($donnees['actions'] as $nom => $valeur) {
            if ($valeur[1]) {
                $liste_txt .= '- ' . $valeur[0] . '\n';
                $liste_html .= '<li>' . $valeur[0] . '</li>';
            }
        }
        $liste_html = $liste_html . '</ul>';

        $message_txt = $donnees['champs']['prenom'] . ' ' . $donnees['champs']['nom'] . ' <' . $donnees['champs']['email'] . '> ' . 'souhaiterait :\n' . $liste_txt;
        $message_html = '<html><head></head><body><p>' . $donnees['champs']['prenom'] . ' ' . $donnees['champs']['nom'] . ' (' . $donnees['champs']['email'] . ') ' . 'souhaiterait :' . $liste_html . '</p></body></html>';

        require_once __DIR__ . '/../modeles/envoyer_email.php';

        $email_envoye = envoyer_email($to, $from, $sujet, $message_txt, $message_html);

        if ($email_envoye) {
            unset($donnees);
        }
    }
} else {
    unset($donnees);
}

require_once __DIR__ . '/../vues/contact.php';

<?php
$page_courante = PAGE_ACTIONS;

require_once __DIR__ . '/../modeles/recuperer_article.php';

if (empty($_GET['id'])) {
    $articles = recuperer_article();

    // On calcule le nombre de pages necessaires a l'affichage de tous les articles
    if (!empty($articles) && (NB_ARTICLES_PAR_PAGE != 0)) {
        // Le nombre de pages est egal a la division entiere de sa taille par le nombre d'articles a afficher par page
        $nb_pages = ceil(sizeof($articles) / NB_ARTICLES_PAR_PAGE);
    } else {
        $nb_pages = 1;
    }

    require_once __DIR__ . '/../vues/actions.php';
} else {
    $article = recuperer_article($_GET['id']);
    $article = $article[0];

    require_once __DIR__ . '/../vues/actions_detail.php';
}

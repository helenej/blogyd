<?php
class BDD extends PDO {
    private static $_instance;

    public function __construct() {
    }

    public static function getInstance() {
        if (!isset(self::$_instance)) {
            try {
                self::$_instance = new PDO(DSN, USERNAME, PASSWD);
                self::$_instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
            } catch (PDOException $e) {
                die($e->getMessage());
            }
        }
        return self::$_instance;
    }

}

<?php
require_once './config/constantes.inc.php';
require_once './library/BDD.inc.php';

if (!empty($_GET['page']) && is_file('./controleurs/' . $_GET['page'] . '.php')) {
    require_once './controleurs/' . $_GET['page'] . '.php';
} else {
    require_once './controleurs/accueil.php';
}

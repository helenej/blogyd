<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
<link rel="stylesheet" href="./css/style.css" />
<link rel="stylesheet" href="./css/style_accueil.css" />
<link rel="icon" type="image/png" href="./images/favicon.png" />
<!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="./images/favicon.ico" /><![endif]-->
<title>Raisons d'agir</title>
</head>
<!--[if IE 6 ]><body class="ie6 old_ie"><![endif]-->
<!--[if IE 7 ]><body class="ie7 old_ie"><![endif]-->
<!--[if IE 8 ]><body class="ie8"><![endif]-->
<!--[if IE 9 ]><body class="ie9"><![endif]-->
<!--[if !IE]><!-->
<body>
 <!--<![endif]-->
 <div id="page">
  <?php
  include './vues/header.php';
  ?>
  <div id="bloc_page">
   <div id=contenu>
    <section>
     <h1>Lorem ipsum dolor</h1>
     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque
      vitae mauris vel ipsum ultrices luctus. Suspendisse ut semper
      orci. Sed at nunc neque, vitae venenatis orci. Phasellus varius
      diam et velit imperdiet in porttitor quam facilisis. Etiam enim
      felis, mattis in fermentum sagittis, malesuada dignissim eros.</p>
     <p>Duis non pretium dolor. Morbi dictum tristique sem, sed accumsan
      mauris pellentesque non. Donec ultricies hendrerit dolor, vitae
      egestas nulla hendrerit eu. Quisque porta pellentesque ante vel
      semper.</p>
     <p>Etiam mauris justo, egestas id lobortis et, vulputate quis
      neque. Maecenas a est vel quam consequat blandit. Vestibulum
      vehicula sagittis tristique. Aliquam egestas dolor nec tellus
      pretium pulvinar. Sed in augue sagittis mauris eleifend tincidunt.
      Donec non eleifend nisl. Praesent vel lorem lectus. Pellentesque
      vitae purus quam, eu imperdiet nisl.</p>
     <div id="signature">Yann Drevet</div>
    </section>
   </div>
   <aside>
    <h1>Restez informé</h1>
    <ul>
     <li><a href="#"><img src="./images/fb1.png" alt="Facebook" /> </a>
     </li>
     <li><a href="#"><img src="./images/twt1.png" alt="Twitter" /> </a>
     </li>
    </ul>
    <h1>Vous souhaitez agir ?</h1>
   </aside>
  </div>
  <?php
  include './vues/footer.php';
  ?>
 </div>
</body>
</html>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
<link rel="stylesheet" href="./css/style.css" />
<link rel="icon" type="image/png" href="./images/favicon.png" />
<!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="./images/favicon.ico" /><![endif]-->
<title>Raisons d'agir</title>
</head>
<!--[if IE 6 ]><body class="ie6 old_ie"><![endif]-->
<!--[if IE 7 ]><body class="ie7 old_ie"><![endif]-->
<!--[if IE 8 ]><body class="ie8"><![endif]-->
<!--[if IE 9 ]><body class="ie9"><![endif]-->
<!--[if !IE]><!-->
<body>
 <!--<![endif]-->
 <div id="page">
  <?php
  include './vues/header.php';
  ?>
  <div id="bloc_page">
   <div id=contenu>
    <section>
     <p>Page en construction</p>
    </section>
   </div>
   <aside></aside>
  </div>
  <?php
  include './vues/footer.php';
  ?>
 </div>
</body>
</html>

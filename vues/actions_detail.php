<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<!--[if lt IE 9]>
<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" href="./css/style.css" />
<link rel="stylesheet" href="./css/style_actions_detail.css" />
<link rel="icon" type="image/png" href="./images/favicon.png" />
<!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="./images/favicon.ico" /><![endif]-->
<title>Raisons d'agir</title>
</head>
<!--[if IE 6 ]><body class="ie6 old_ie"><![endif]-->
<!--[if IE 7 ]><body class="ie7 old_ie"><![endif]-->
<!--[if IE 8 ]><body class="ie8"><![endif]-->
<!--[if IE 9 ]><body class="ie9"><![endif]-->
<!--[if !IE]><!-->
<body>
 <!--<![endif]-->
 <div id="page">
  <?php
  include './vues/header.php';
  ?>
  <div id="bloc_page">
   <div id=contenu>
    <section>
     <article id="article">
      <h1>
       <?php echo htmlspecialchars($article['titre']); ?>
      </h1>
      <h2>
       Posté le
       <?php echo $article['date_creation_fr']; ?>
      </h2>
      <?php if (!is_null($article['image'])) {
          ?>
      <img alt="Illustration"
       src="<?php echo './images/images_articles/' . $article['image']; ?>"
       id="img_detail">
      <?php
      }
      echo $article['contenu'];
      ?>
      <br />
      <?php
      if (empty($_GET['num_page'])) {
        // Si la variable $_GET['page'] n'est pas definie (premiere connexion), on est par défaut sur la page 1
        $num_page = 1;
    } else {
        // Sinon la page courante est celle definie par la variable $_GET['page']
        $num_page = $_GET['num_page'];
    }
    ?>
      <a href="./action-<?php echo $num_page; ?>.html#contenu">Retour à
       la liste des articles</a>
     </article>
     <article>
      <!-- Insertion d'un article vide pour le clear -->
     </article>
    </section>
   </div>
   <aside></aside>
  </div>
  <?php
  include './vues/footer.php';
  ?>
 </div>
</body>
</html>

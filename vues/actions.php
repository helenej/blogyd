<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
<link rel="stylesheet" href="./css/style.css" />
<link rel="stylesheet" href="./css/style_actions.css" />
<link rel="icon" type="image/png" href="./images/favicon.png" />
<!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="./images/favicon.ico" /><![endif]-->
<title>Raisons d'agir</title>
</head>
<!--[if IE 6 ]><body class="ie6 old_ie"><![endif]-->
<!--[if IE 7 ]><body class="ie7 old_ie"><![endif]-->
<!--[if IE 8 ]><body class="ie8"><![endif]-->
<!--[if IE 9 ]><body class="ie9"><![endif]-->
<!--[if !IE]><!-->
<body>
 <!--<![endif]-->
 <div id="page">
  <?php
  include './vues/header.php';
  ?>
  <div id="bloc_page">
   <div id=contenu>
    <?php
    if (empty($_GET['num_page'])) {
// Si la variable $_GET['page'] n'est pas definie (premiere connexion), on est par défaut sur la page 1
$num_page = 1;
}
else {
					// Sinon la page courante est celle definie par la variable $_GET['page']
					$num_page = $_GET['num_page'];
					}

					if ($num_page == 1) {
					?>
    <section>
     <h1>Lorem ipsum dolor</h1>
     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque
      vitae mauris vel ipsum ultrices luctus. Suspendisse ut semper
      orci. Sed at nunc neque, vitae venenatis orci. Phasellus varius
      diam et velit imperdiet in porttitor quam facilisis. Etiam enim
      felis, mattis in fermentum sagittis, malesuada dignissim eros.</p>
    </section>
    <?php
                    }
                    ?>
    <section>
     <?php
     for ($i = ($num_page - 1) * NB_ARTICLES_PAR_PAGE; $i <= ($num_page - 1) * NB_ARTICLES_PAR_PAGE + NB_ARTICLES_PAR_PAGE - 1; $i++) {
if (!empty($articles[$i]) && ($articles[$i]['page'] == PAGE_ACTIONS)) {
// Si il y a un article a l'index $i et qu'il faut l'afficher sur cette page
?>
     <article>
      <?php if (!is_null($articles[$i]['image'])) {
          ?>
      <img alt="Illustration"
       src="<?php echo './images/images_articles/' . $articles[$i]['image']; ?>">
      <?php
      }
      ?>
      <h1>
       <?php echo htmlspecialchars($articles[$i]['titre']); ?>
      </h1>
      <h2>
       Posté le
       <?php echo $articles[$i]['date_creation_fr']; ?>
      </h2>
      <p>
       <?php
       $contenu = strip_tags($articles[$i]['contenu']);
       // On supprime les balises HTML et PHP de la chaine pour n'afficher que le texte non formate sur cette page
       $contenu_decoupe = wordwrap($contenu, 150, '***', true);
       // On insere des marqueurs dans la chaine
       $tab_contenu_decoupe = explode('***', $contenu_decoupe);
       // On decoupe la chaine suivant ces marqueurs
       $contenu_tronque = $tab_contenu_decoupe[0];
       // On recupere le premier element decoupe
       echo $contenu_tronque . ' [...] <a href="./action-' . ($articles[$i]['current_id']) . '-' . $num_page . '.html#article">Lire la suite</a>';
       ?>
      </p>
     </article>
     <?php
                    } // Fin du if (!empty($articles[$i]))
                    } // Fin de la boucle for
                    ?>
     <article>
      <!-- Insertion d'un article vide pour le clear -->
     </article>
     <div class="num_page">
      <?php
      echo 'Page : ';
      for ($p = 1; $p <= $nb_pages; $p++) {
                        if ($p == $num_page) {
                            $num_page_url = $p;
                        } else {
                            $num_page_url = '<a href="./action-' . $p . '.html#contenu">' . $p . '</a>  ';
                        }
                        echo $num_page_url;
                    }
                    ?>
     </div>
    </section>
   </div>
   <aside>
    <h1>Lyon en images</h1>
   </aside>
  </div>
  <?php
  include './vues/footer.php';
  ?>
 </div>
</body>
</html>

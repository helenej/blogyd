<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
<link rel="stylesheet" href="./css/style.css" />
<link rel="stylesheet" href="./css/style_contact.css" />
<link rel="icon" type="image/png" href="./images/favicon.png" />
<!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="./images/favicon.ico" /><![endif]-->
<title>Raisons d'agir</title>
</head>
<!--[if IE 6 ]><body class="ie6 old_ie"><![endif]-->
<!--[if IE 7 ]><body class="ie7 old_ie"><![endif]-->
<!--[if IE 8 ]><body class="ie8"><![endif]-->
<!--[if IE 9 ]><body class="ie9"><![endif]-->
<!--[if !IE]><!-->
<body>
 <!--<![endif]-->
 <div id="page">
  <?php
  include './vues/header.php';
  ?>
  <div id="bloc_page">
   <div id=contenu>
    <section>
     <h1>
      <img src="./images/icone_lettre.png" alt="" id="icone_lettre" />Lorem
      ipsum dolor sit amet, consectetur adispicing elit.
     </h1>
     <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras
      lectus enim, aliquet eu ultricies in, eleifend et erat.
      Suspendisse suscipit ligula eleifend nisl condimentum id volutpat
      lacus sagittis. Phasellus id libero justo. Phasellus hendrerit
      bibendum nulla, mattis pretium diam lacinia eu.</p>
     <!--[if IE]>
     <?php
     if (isset($donnees) && (empty($donnees['champs']['nom']) || empty($donnees['champs']['prenom']) || ($donnees['champs']['email'] === ''))) {
         echo '<p class="message_erreur">Tous les champs sont obligatoires</p>';
     }
     if (isset($donnees) && ($donnees['champs']['email'] === false)) {
         echo '<p class="message_erreur">E-mail invalide</p>';
     }
     ?>
     <![endif]-->
     <form action="./page-contact.html" method="post">
      <p>
       <label for="nom">Nom :</label> <input type="text" name="nom"
        size="45"
        <?php
        if (isset($donnees)) {
                                        if (!empty($donnees['champs']['nom']))
                                            echo 'value="' . $donnees['champs']['nom'] . '"';
                                        else
                                            echo 'placeholder="Vous devez renseigner votre nom"';
                                    }
                                    ?>
        required />
      </p>
      <p>
       <label for="prenom">Prénom :</label> <input type="text" size="45"
        name="prenom"
        <?php
        if (isset($donnees)) {
                                        if (!empty($donnees['champs']['prenom']))
                                            echo 'value="' . $donnees['champs']['prenom'] . '"';
                                        else
                                            echo 'placeholder="Vous devez renseigner votre prénom"';
                                    }
                                    ?>
        required />
      </p>
      <p>
       <label for="email">E-mail :</label> <input type="email" size="45"
        name="email"
        <?php
        if (isset($donnees)) {
                                        if (!empty($donnees['champs']['email']))
                                            echo 'value="' . $donnees['champs']['email'] . '"';
                                        else if ($donnees['champs']['email'] === '')
                                            echo 'placeholder="Vous devez renseigner votre E-mail"';
                                        else if ($donnees['champs']['email'] === false)
                                            echo 'placeholder="E-mail invalide"';
                                    }
                                    ?>
        required />
      </p>
      <table>
       <tr>
        <td><label for="agir_t"><img src="./images/icone_1.png"
          alt="Agir sur le terrain" /> </label></td>
        <td><label for="agir_w"><img src="./images/icone_2.png"
          alt="Agir sur le web" /> </label></td>
        <td><label for="redacteur"><img src="./images/icone_3.png"
          alt="Devenir rédacteur" /> </label></td>
        <td><label for="informe"><img src="./images/icone_4.png"
          alt="Se tenir informé" /> </label></td>
        <td><label for="donner"><img src="./images/icone_5.png"
          alt="Donner à l'association" /> </label></td>
       </tr>
       <tr>
        <td><input type="checkbox" name="agir_t" id="agir_t"
        <?php
        if (isset($donnees) && $donnees['actions']['agir_t'][1])
            echo 'checked=checked';
        ?> /> <label for="agir_t"></label></td>
        <td><input type="checkbox" name="agir_w" id="agir_w"
        <?php
        if (isset($donnees) && $donnees['actions']['agir_w'][1])
            echo 'checked=checked';
        ?> /> <label for="agir_w"></label></td>
        <td><input type="checkbox" name="redacteur" id="redacteur"
        <?php
        if (isset($donnees) && $donnees['actions']['redacteur'][1])
            echo 'checked=checked';
        ?> /> <label for="redacteur"></label></td>
        <td><input type="checkbox" name="informe" id="informe"
        <?php
        if (isset($donnees) && $donnees['actions']['informe'][1])
            echo 'checked=checked';
        ?> /> <label for="informe"></label></td>
        <td><input type="checkbox" name="donner" id="donner"
        <?php
        if (isset($donnees) && $donnees['actions']['donner'][1])
            echo 'checked=checked';
        ?> /> <label for="donner"></label></td>
       </tr>
      </table>
      <?php dsp_crypt(0, 'Réactualiser'); ?>
      <input type="submit" name="envoyer" value="Envoyer" />
     </form>
    </section>
   </div>
   <aside>
    <h1>Restez informé</h1>
    <ul>
     <li><a href="#"><img src="./images/fb1.png" alt="Facebook" /> </a>
     </li>
     <li><a href="#"><img src="./images/twt1.png" alt="Twitter" /> </a>
     </li>
    </ul>
   </aside>
  </div>
  <?php
  include './vues/footer.php';
  ?>
 </div>
 <?php
 if (isset($email_envoye) && $email_envoye) {
            echo '
        <script>
        alert("Message envoyé")
        </script>';
        }
        ?>
</body>
</html>

<header>
 <a href="./"> <img alt="Logo" src="./images/logo.png" id="logo" />
 </a>
 <nav>
  <ul>
   <li><a href="./"
   <?php
   if ($page_courante == PAGE_ACCUEIL)
       echo 'id="page_courante"';
   ?>>Accueil</a>
   </li>
   <li><a href="./page-raisons-d-agir.html"
   <?php
   if ($page_courante == PAGE_RAISONS_DAGIR)
       echo 'id="page_courante"';
   ?>>Raisons d'agir</a>
   </li>
   <li><a href="./page-actions.html"
   <?php
   if ($page_courante == PAGE_ACTIONS)
       echo 'id="page_courante"';
   ?>>Actions</a>
   </li>
   <li><a href="./page-un-autre-bron.html"
   <?php
   if ($page_courante == PAGE_UN_AUTRE_BRON)
       echo 'id="page_courante"';
   ?>>Un autre Bron</a>
   </li>
   <li><a href="./page-contact.html"
   <?php
   if ($page_courante == PAGE_CONTACT)
       echo 'id="page_courante_contact"';
   ?>>A vous d'agir !</a>
   </li>
  </ul>
 </nav>
 <img alt="" src="./images/bandeau_photo.png" />
</header>
